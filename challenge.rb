include Math
require 'net/http'
require 'json'

# challenge 1
def getPiDecimal()
# your code here
	# PI value
	pi_value = PI
	# integer and decimal in array 
	arrayNumber = pi_value.to_s.split(".")
	# number in tenth position
	result = arrayNumber[1][9].to_i
	return result;
end

# challenge 2
def getSumEvens(a=[1,2,3,4,5,6])
# your code here
	# start in 0
	result = 0
	# array loop
	for n in a do
		# if even
		if n % 2 == 0
			result += n
		end
	end
	return result;
end

# challenge 3
def getOrderedVowels(s="just a testing")
# your code here
	# vowels array
	arrayVowels = ["a", "e", "i", "o", "u"]	
	# string size
	textLength = s.length
	# start value
	result = ""
	# loop letter - text
	for i in 0..textLength do
		# if vowel
		if arrayVowels.index(s[i])
			result += s[i]
		end
	end
	return result;
end

# challenge 4
def getFirstId()
# yor code here
	# set URI
	uri = URI('https://jsonplaceholder.typicode.com/users')
	# request
	response = Net::HTTP.get(uri)
	# parte to JSON
	response = JSON.parse(response)
	# get id first user
	result = response[0]['id']
	return result;
end

# DONT EDIT
puts "Running: \n";
puts "challenge 1: "+((getPiDecimal()==5)? "pass" : "fail") + "\n" ;
puts "challenge 2: "+((getSumEvens()==12)? "pass" : "fail" ) + "\n" ;
puts "challenge 3: "+((getOrderedVowels()=="uaei")? "pass" : "fail") + "\n" ;
puts "challenge 4: "+((getFirstId()==1)? "pass" : "fail" )+ "\n" ;