- 1) que tanto conoces sobre procesos de CI/CD?
    Que es una automatización de despliegue para los distintos ambientes que se tengan por proyectos.
- 2) que tanto conoces sobre linux?
    Tengo un conocimiento de Básico - Intermedio por lo que lo uso muy amenudo.
- 3) que te gusta y NO te gusta del lenguaje que usas?
    Me gusta:
        - Comunidad activa de desarrolladores(RUBY).
        - Variedad de librerias.
    No me gusta:
        - Según comunidades de desarrolladores, la popularidad "RUBY" esta decreciendo.
- 4) si llegas a un punto con algo que no sabes como solucionar que haces?
    Investigo y consulta a la comunudad de RUBY.
- 5) que opinas del trabajo remoto, tienes experiencia?
    Vengo trabajando como FreeLance desde el 2013, según mi experiencia esta modadlidad es muy productiva dado que el desarrollador se crea su horario con responsabilidad(disciplina).
- 6) si te dan una instruccion que no comprendes que haces?
    Busco la forma de reunirme presencialmente para aclarar dudas y fortalezer la comunicación.
- 7) que opinas de las "horas extras"?
    Permite mayor productivida y demuestra el interes una persona a su trabajo. 
- 8) cuentame cual es la cosa mas dificil que has resuelto.
    El construir una estructura de proyecto, en la cual todo el equipo pudiera desarrollar sin que, los cambios de uno afecte al otro. 